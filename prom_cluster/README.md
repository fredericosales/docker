# Prometheus

## Como configurar e monitorar orquestração de containers com kubernetes, prometheus e grafana

Esse tutorial vai guia-lo atraves da configuração do prometheus em um cluster kubernetes.

Nessa configuração podemos coletar metricas de forma atomatizada utilizando o prometheus.

### Sobre o prometheus

O *prometheus* é um serviço *open source* de alta escalabilidade. Ele fornece a monitoração do serviço de orquestração de *containers* *kubernetes*, dentre outros serviços fora do escopo deste tutorial.

Descrever o *prometheus* também está fora do tema deste tutorial. Entrentanto, sua documentação pode ser encontrada no endereço [prometheus](https://prometheus.io/docs/prometheus/latest/getting_started/). Ou acompanhar todos os videos a partir desse [link](https://www.youtube.com/channel/UC4pLFely0-Odea4B2NL1nWA/videos). Em toda via, existem alguns pontos a se descutir.

1. __Coleta de metricas:__ o *prometheus* utiliza um modelo que coleta dados sobre o protocolo *HTTP*. Observe que existe também uma opção de fazer envios com o *prometheus* utilizando o metodo *Pushgateway*. Entretanto quando esse metodo é utilizado ele não pode coletar dados;

2. __metric endpoint:__ para os sistemas que você quer monitorar utilizando o *prometheus*. Ele deve expor as metricas em um *endpoint /metrics*. Ele utiliza esse *endpoint* para buscar as metricas em periodos regulares de tempo;

3. __PromQL:__ é uma linguagem muito flexivel para consultas utilizada pelo *prometheus* para exibir as metricas baixadas em um *dashboard*;

4. __prometheus exporters:__ são bibliotecas utlizadas para converter metricas existentes em aplicações de terceiros para o formato compreencível para o *prometheus*. Existem muitos exemplos da comunidade *open source* e oficiais. Um exemplo é o *node exporter*. Sendo assim , ele converte todas as metricas do sistema *linux* para o formato *prometheus* fator que possibilita fazer consultas de forma sistemática e visualiza-las em um *dashboard* customizado;

5. __TSDB (time series database):__ o *prometheus* utiliza TSDB para armazenar dados de forma eficiente. Por padrão, todos os dados são armazenados localmente. Entretanto, para evitar um ponto de falha uníco, existem opções de integrar armazenamentos remotos de forma a garantir a segurança dos dados coletados.

### Arquiterura do prometheus

Arquiterura simplificada do prometheus.

![arquitetura em alto nível do prometheus](img/architecture-min.png)
Fonte: https://prometheus.io

### Monitoramento

Para a camada de monitoramento precisamos dos seguintes componentes.

1. __Servidor prometheus__;

2. __Gerenciamento de alertas__;

3. __Grafana__.

De forma geral nossa solução vai se comportar conforme a arquitetura de alto nível a seguir.

![arquitetura orquestrada](img/camada_de_monitoramento.png)

### Configurando o monitoramento com kebernetes

Fonte: [próprio autor](https://fredericosales.eng.br).

Eu assumo que você já tenha um cluster configurado rodando com *kubectl* na sua estação de trabalho. Caso contratio no [google](https://google.com/) você vai encontrar diversos tutoriais para deixar sua estação de trabalho pronta para esse tutorial. Aconselho a utilizar o [minikube](https://devopscube.com/kubernetes-minikube-tutorial/) por ser fácil de instalar e leve para rodar mesmo em máquinas modestas.

### Conectar ao orquestrador kubernetes

Caso opte por utilizar o google cloud GKE, você vai precisar rodar os seguintes comandos no shell google. Observe, você vai precisar ter privilégios para criar um cluster na sua conta google.

```bash
ACCOUNT=$(gcloud info --format='value(config.account)')
kubectl create clusterrolebinding owner-cluster-admin-binding \
    --clusterrole cluster-admin \
    --user $ACCOUNT
```

### Prometheus kubernetes Manfiest files

Todos os arquivos de configuração estão disponibilizados no [gitlab](https://gitlab.com/fredericosales/docker.git).

Baixe os manifestos com o git clone.

```bash
git clone https://gitlab.com/fredericosales/docker.git
```

Após baixa-los vamos prosseguir com o *deploy* dos serviços.

### Criar o namespace & clusterRole

A primeira etapa é usar o *kubectl* para criar o *namespace* monitoramento.

```bash
kubectl create namespace monitoramento
````

O *prometheus* utiliza a *API* do *kubernetes* para ler todas as metricas dos nós, pods, deployments, etc. Por essa razaão, nós criamos uma politica *RBAC* com acesso para leitura para podermos conectar a politica ao namespace monitoramento.

1. __Primeiro passo:__ criar o clusterRole com o manifesto.

    ```bash
    kubectl create -f clusterRole.yaml
    ```

2. __Segundo passo:__ criar os arquivos de configuração para o prometheus.
    - Todas as configurações do prometheus estão no arquivo prometheus.yml ou prometheus.yaml dependendo do seu sistema operacional. Observe que os arquivos contendo as regras para alertas vão ser encontradas no arquivo prometheus.rules.

    - Ao externar as configurações prometheus para um cluster kubernetes, você não precisa construir uma imagem. Entretanto você vai precisar adicionar ou remover uma configuração. Lembre-se que você precisa reiniciar os pods prometheus para que qualquer alteração nos arquivos de configuração tenham efeito.

    - O mapa de configurações contendo todas as regras para coletar dados e produzir alertas são montadas dentro do *container* no caminho */etc/prometheus/prometheus.yml* e */etc/prometheus/prometheus.rules*.
  
    ```bash
    kubectl create -f config-map.yaml
    ```

### Criar o prometheus deployment

Criar o deployment no namespace monitoramento.

```bash
kubectl create -f prometheus-deployment.yaml
```

Verifique se esta tudo ok com o comando:

```bash
kubctl get deployments --namespace=monitoramento
```

### Conectar o prometheus ao dashboard

Você pode acessar ao dashboard de três maneiras diferentes.

1. Utilizando o *kubeclt forwarding*;

2. Expondo o serviço com *NodePort* ou *Load Balancer*, haproxy por exemplo;

3. Adicionando um *ingress object* se você possui um *ingress controller deployed*.

### Metodo 1

Utilizando o *kubctl forwarding*, você pode acessar o pod da sua estação local por uma porta previamente selecionada no seu *localhost*. Esse metodo é normalmente utilizado para fazer o debug. Os demais metódos vou deixar para que vocês pesquizem.

Verificando o estado do pod

```bash
kubectl get pods --namespace=monitoring
NAME                                       READY     STATUS    RESTARTS   AGE
prometheus-monitoramento-3331088907-hm5n1  1/1       Running   0          5m
```

Fazendo o *port forwarding*

```bash
kubectl port-forward prometheus-monitoramento-3331088907-hm5n1 8080:9090 -n monitoring
```

Por ultimo vamos acessar o servidor atraves do endereço http://localhost:8080. Se tudo ocorreu de forma como esperado você vai ver a página inicial do *prometheus*.

### Considerações

Para um servidor de produção existem mais configurações e paramentros a serem considerados e que não foram citados nesse tutorial, não consideramos por exemplo alta disponibilidade, e armazenameto de logs. Fatores que influem bastante na infra utilizada.

### Conclução

Esse pequeno tutorial cobre uma pequena parte das funcionalidades da utilização do *grafana* e *prometheus* para monitoramento e envio de alertas.

Recomendo que se faça a leitura mais aprofundada do gui de inicio rápido *prometheus* no [link](https://prometheus.io/docs/prometheus/latest/getting_started/).
