# Prometheus

## Como configurar e monitorar orquestração de containers com kubernetes, prometheus e grafana

Esse tutorial vai guia-lo atraves da configuração do prometheus em um cluster kubernetes.

Nessa configuração podemos coletar metricas de forma atomatizada utilizando o prometheus.

### Sobre o prometheus

O prometheus é um serviço opensource de alta escalabilidade. Ele fornece a monitoração do serviço de orquestração de containers do kubernetes, dentre outros serviços fora do escopo deste tutorial.

Descrever o prometheus também está fora do tema deste tutorial. Entrentanto, sua documentação pode ser encontrada no endereço [prometheus]([https://](https://prometheus.io/docs/prometheus/latest/getting_started/)). Ou acompanhar todos os videos a partir desse [link](https://www.youtube.com/channel/UC4pLFely0-Odea4B2NL1nWA/videos). Em toda via, existem alguns pontos a se descutir.

1. __Coleta de metricas:__ o prometheus utiliza um modelo que coleta dados sobre o protocolo HTTP. Observer que que existe também uma opção de fazer envios com o prometheus utilizando o metodo Pushgateway. Entretanto quando esse metodo é utilizado o prometheus não pode coletar dados.

2. __metric endpoint:__ para os sistemas que você quer monitorar utilizando o prometheus. Ele deve expor as metricas em um endpoint /metrics. Ele utilizanda esse endpoint para buscas as metricas em periodos regulares de tempo.

3. __PromQL:__ é uma linguagem muito flexivel para consultas utilizada pelo prometheus para exibir as metricas baixadas em um dashboard.

4. __prometheus exporters:__ são bibliotecas utlizadas para converter metricas existentes em aplicações de terceiros para o formato prometheus. Existem muitos exemplos da comunidade opensource e oficiais. Um exemplo é o node exporter. Ele converte todas as metricas do sistema linux para o formato prometheus o que possibilita fazer consultas de forma sistemática e visualiza-las em um dashboard customizado.

5. __TSDB (time series database):__ o prometheus utiliza TSDB para armazenar dados de forma eficiente. Por padrão, todos os dados são armazenados localmente. Entretanto, para evitar um ponto de falha, existem opções de integrar armazenamentos remotos.

### Arquiterura do prometheus

Arquiterura simplificada do prometheus.

![arquitetura em alto nível do prometheus](img/architecture-min.pnp)

## Run the service

```bash
docker-compose -f prom.yml up -d
```

## The entrypoint.sh script

```bash
prometheus --config.file=prometheus.yml
```
